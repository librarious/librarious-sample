require 'mechanize'
require 'result_book'
require 'book_formats'
require 'uri'
require 'cgi'
require 'libinterface/form_submit_interface'

# Implements the interface to the Encore library system
class EncoreInterface < FormSubmitInterface

  attr_accessor :format_string, :suite


  def initialize(url_prefix, opts)
    super

    # build a hash with all of the different css search strings needed
    # for differences in each of the encore suites
    @suite_customs = {
      "cobalt" => {
        results: "div.dpBibTitle",
        title: "span.title"},
      "beta" => {
        results: "div.dpBibTitle",
        title: "span.title",
      }
    }
  end

  # overrides parent build_url method by just returning the
  # url prefix plus the search path.
  # The opts hash is not used for this interface
  def build_url(book)
    return @url_prefix + "/?lang=eng"
  end


  # overrides the abstract handle_form
  def handle_form(page, book)
    f = page.forms.first
    f.searchString = "t:(" + book.title + ") a:(" + book.author + ")"
    return f
  end

  def parse_library_listings(doc, search_url)

    # dump page to file
    # f = open("encore_search_results.html", "w")
    # f.truncate(0)
    # f.write(Nokogiri::HTML(doc.content))
    # f.close

    book_list = []

    # for encore, disregard the original search url and use the one 
    # from the document itself
    search_url = doc.uri.to_s

    # get the suite variable from the url so we can see which
    # encore suite this is.  default to cobalt
    # suite_hash = CGI.parse(URI.parse(search_url).query)["suite"]
    suite_hash = CGI.parse(URI.parse(search_url).query)["suite"]
    if suite_hash
      suite = suite_hash[0]
      if !@suite_customs.keys.include? suite
        # it's an unknown suite, so default to cobalt
        suite = "cobalt"
      end
    else
      # coultn't find the suite, so default to cobalt
      suite = "cobalt"
    end

    # get all of the results elements.  They differ by suite
    results = doc.parser.css(@suite_customs[suite][:results])

    results.each do |result|

      # get title
      title = result.css(@suite_customs[suite][:title]).text.strip

      # get author
      author_elem = result.css("div.dpBibAuthor")
      if author_elem
        author = author_elem.text.strip
      else
        author = nil
      end

      # get format
      format_elem = result.css("span.itemMediaDescription")
      if format_elem
        format_text = format_elem.text.strip.downcase
      else
        format_text = "UNKNOWN"
      end

      if ["book", "books", "large print", "paperback", "adult book", "teen book"].include? format_text
        format = BookFormats::BOOK
      elsif format_text.include? "ebook"
        format = BookFormats::EBOOK
      elsif format_text.include? "audio"
        format = BookFormats::OTHER
      elsif format_text.include? "video"
        format = BookFormats::OTHER
      elsif format_text.include? "cd"
        format = BookFormats::OTHER
      elsif format_text.include? ""
        format = BookFormats::OTHER
      elsif format_text.include? ""
        format = BookFormats::OTHER
      else
        format = BookFormats::UNKNOWN
      end

      # tidy our title and author in case we got something invalid
      title = title.mb_chars.tidy_bytes unless title.nil?
      author = author.mb_chars.tidy_bytes unless author.nil?

      # only add to results if it is a book, ebook, or unknown
      book = ResultBook.new({ :title => title,
                              :author => author,
                              :format => format,
                              :url => search_url})
      if book.supported?
        book_list << book
      end

    end

    return book_list
  end

end

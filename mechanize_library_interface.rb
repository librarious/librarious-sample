require 'mechanize'
require 'libinterface/library_interface'

# parent class for getting data from a library via mechanize
class MechanizeLibraryInterface < LibraryInterface

  def initialize(url_prefix, opts)
    super
  end

  def get_library_listings(book)
    url = build_url(book)

    doc = get_web_listing(url, book)

    return parse_library_listings(doc, url)
  end

  # Constructs and returns the url for searching
  # Should be implemented by each subclass!
  def build_url(book)
    return ""
  end

  # makes the web call and returns an html document
  # this is a default case of just calling the url directly
  # should be overriden by subclass if a more complicated model is needed
  # such as multiple calls with a session or a form post
  def get_web_listing(url, book)
    agent = Mechanize.new
    doc = agent.get(url)
    return doc
  end

  # Parses the search results and returns a list of
  # ResultBooks
  def parse_library_listings(doc, search_url)
    return []
  end
end

require 'mechanize'
require 'result_book'
require 'book_formats'
require 'uri'
require 'cgi'
require 'libinterface/mechanize_library_interface'

# Generic interface for a library system that uses mechanize
# to load a page with a search form, then submit the completed
# form to do the search
class FormSubmitInterface < MechanizeLibraryInterface

  attr_accessor :format_string, :suite


  def initialize(url_prefix, opts)
    super
  end

  # takes a page and completes the search form
  # meant to be overridden by specific library interface
  def handle_form(page, book)
  end

  def get_web_listing(url, book)
    agent = Mechanize.new

    searchpage = agent.get(url)
    f = handle_form(searchpage, book)
    resultpage = agent.submit(f, f.buttons.first)

    return resultpage
  end

end

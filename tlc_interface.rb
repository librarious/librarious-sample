require 'libinterface/library_interface'

# parent class for getting data from a library via mechanize
class TlcInterface < LibraryInterface

  def initialize(url_prefix, opts)
    super
  end

  def get_library_listings(book)
    uri = URI(@url_prefix + '/search')
    http = Net::HTTP.new(uri.host, uri.port)
    req = Net::HTTP::Post.new(uri)
    req.content_type = 'application/json'
    req.body = build_json_body(book)
    resp = http.request(req)

    respdata = JSON.parse(resp.body)
    results = respdata["resources"]

    search_url = build_search_url(book.title, book.author, @url_prefix)
    book_list = []
    results.each do |result|
      title = result["shortTitle"]
      author = result["shortAuthor"]

      format_text = result["format"].strip.downcase
      if format_text == "ebook"
        format = BookFormats::EBOOK
      elsif format_text == "book"
        format = BookFormats::BOOK
      else
        format = BookFormats::OTHER
      end

      # only add to results if it is a book, ebook, or unknown
      book = ResultBook.new({ :title => title,
                              :author => author,
                              :format => format,
                              :url => search_url})
      if book.supported?
        book_list << book
      end
    end

    return book_list
  end

  def build_json_body(book)
    #
    # !!!!!
    # Build the json body from a string instead of building a hash and returning 
    # to_json since they require some parts of the json to be escped, but others 
    # to not be escaped.  Easier and safer to just build it manually
    # 
    data = '{"searchTerm":"\"' + 
      book.title + '\" \"' + 
      book.author + '\"","startIndex":0,"hitsPerPage":12,"facetFilters":[],"sortCriteria":"Relevancy","targetAudience":"","addToHistory":true,"dbCodes":[],"audienceCharacteristicsFilters":[],"readingLevelFilters":null}'

  end

  def build_search_url(title, author, url_prefix)
    if title
      title_str = '"' + title + '"'
    else
      title_str = ''
    end

    if author
      author_str = '"' + author + '"'

      #insert a leading space if both title and author are there
      author_str = author_str.insert(0, ' ') unless title.nil?
    else
      author_str = ''
    end

    search_url = url_prefix +
      '/#section=search&term=' +
      title_str +
      author_str + 
      '&page=0&sortKey=Relevancy&db=ls2pac&facetFilters=[]'
  end
end
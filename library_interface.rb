# parent class for getting data from a library
class LibraryInterface

  attr_accessor :url_prefix, :opts

  def initialize(url_prefix, opts)
    @url_prefix = url_prefix
    @opts = opts
  end

  def get_library_listings(book)
    return []
  end

end

# Sample code from librario.us

This is a snippet of code from the [librario.us](http://app.librario.us/) site, meant to serve as a coding sample of Dave Pearson for Pearson Technology Ventures.

## Framework for searching different library systems
This code servers as a framework for searching different library systems for a list of books that match title and author keywords.  It uses a set of abstract classes as the core of the framework to encompass common functionality, with a concrete subclass for a given library system to implement behavior specific to that library system.

## Framework classes
The key framework classes are:

+  **library\_interface**  This is the top-level class of the framework.  It supports basic initialization from library details that are passed in, and a generic method to retrieve library results.  The concrete classes extending this class typically are for library systems that use a REST interface, with the resulting book list constructed from the JSON results.

+  **mechanize\_interface**  This is a subclass of library\_interface that supports a basic http get with query parameters via mechanize.  It provides the following methods that a concrete subclass for library system needs to override:

    +  **build\_url**  This should be overriden to construct the url query string specific to that library system

    +  **parse\_library\_listings**  This should be overriden to parse the html results specific to that library system

+  **form\_submit\_interface**  This is a subclass of mechanize\_interface that extends mechanize\_interface in order to support a form submit via http post, by overriding the get\_web\_listings method.  It provides the following method that a concrete subclass for a library system needs to override:

    +  **handle\_form**  This should be overriden to parse the initial html page, in order to find the correct form on the page for the search and populate it with the right query string(s).


## Sample classes for specific library systems
Two sample classes for implementing a specific interface to a library system are included in this sample:

+  **encore\_interface**  This is an interface to the encore library system.  It extends the form\_submit\_interface since the search on this system is form-based.

    +  [Example library:  Mountain View Public Library](http://encore.mountainview.gov/iii/encore/search/C__St%3A%281984%29%20a%3A%28George%20Orwell%29__Orightresult__U?lang=eng&suite=cobalt)

+  **tlc\_interface**  This is an interface to the TLC library interface which extends the top-level library\_interface.  It uses an HTML post with the search parameters in the body in JSON, with the search results also returned in JSON.

    +  [Example library:  Sequoyah Regional Library System](http://webserver.sequoyahregionallibrary.org:8080/#section=search&term="1984" "George Orwell"&page=0&sortKey=Relevancy&db=ls2pac&facetFilters=[])
